
const FIRST_NAME = "Ilie-Adrian";
const LAST_NAME = "Lungu";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value===NaN)
    return NaN;
    if(value===Infinity||value===Infinity)
    return NaN;
    if(value>Number.MAX_SAFE_INTEGER||value<Number.MIN_SAFE_INTEGER)
    return NaN;

    return Number.parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

